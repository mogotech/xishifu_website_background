魔工坊后台系统
### 插件
+ 阿里短信
> http://bbs.dolphinphp.com/?/article/63
+ 七牛云
> http://bbs.dolphinphp.com/?/article/48
+ excel
> 教程 https://miaoqiang.name/archives/dolphinphp-excel-plugins.html

### composer包
+ easyWeChat
> https://www.easywechat.com/docs/3.x/zh-CN/index

## 日志

### 2018.3.28
+ 将easywechat升级到3.x 文档链接也做了相应修改

### 2018.2.27
+ 上传API加入了跨域,可以使用跨域上传
+ 增加了模块示例的菜单

### 2018.2.9 
+ 加入上传的开放API /index/index/upload 字段为 file
+ 增加 mogo_error 统一错误返回码 
+ 增加路由用例
+ 增加新模块用例 包括控制器/模型/验证器 配置

### 2018.2.4
+ 替换了默认的logo

### 2018.1.11
+ 增加杨希成提供的一些公共方法
